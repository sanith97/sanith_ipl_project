function getMatchesPlayedPerYear(dataObject) {//dataObject referring to the matches data
    let noOfMatches = [{
        name : "Total Number of matches played",
        data : []
    }];

    let startingYear = 2008;

    for (let index = 0; index < 10; index ++) {
        let emptyObj = {}
        let matchCount = 0;
        for(let object of dataObject) {
            if(object['season'] == startingYear) {
                matchCount += 1;
            }
        }

        noOfMatches[0]['data'].push(matchCount);
        startingYear += 1;
    }

    return noOfMatches;
}

module.exports = getMatchesPlayedPerYear;

