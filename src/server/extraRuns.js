function getTeamExtraRuns(dataObjectDeliveries, dataObjectMatches, year) {

    if(dataObjectDeliveries === undefined || dataObjectMatches === undefined || year === undefined) {
        return {};
    } 

    if(dataObjectDeliveries.length === 0 || dataObjectMatches.length === 0) {
        return {};
    }

    let teams = [
        {
            name : 'Bangalore',
            data : []
        },
        {
            name : 'Kolkata',
            data : []
        },
        {
            name : 'Mumbai',
            data : []
        },
        {
            name : 'Punjab',
            data : []
        },
        {
            name : 'Pune',
            data : []
        },
        {
            name : 'Delhi',
            data : []
        },
        {
            name : 'Gujarat',
            data : []
        },
        {
            name : 'Rajasthan',
            data : []
        },
        {
            name : 'Chennai',
            data : []
        },
        {
            name : 'Hyderabad',
            data : []
        },    
    ];

    let idObj = (dataObjectMatches.filter(obj => obj['season'] == year));
    let idArray = [];

    idObj.forEach(obj => idArray.push(obj['id']));
    let slicedObj = dataObjectDeliveries.filter(obj => (idArray.includes(obj['match_id'])));

    for(let iplTeam = 0 ; iplTeam < teams.length ; iplTeam++) {
        let returnedVal = getindividualStats(teams[iplTeam]['name']);
        teams[iplTeam]['data'].push(returnedVal);
    }

    
    function getindividualStats(team) {
        let extraRuns = 0;

        for (let obj of slicedObj) {
            if(obj['bowling_team'].includes(team)) {
                extraRuns += Number(obj['extra_runs']);
            }
        }

        return extraRuns;
    }

    return teams;
}

module.exports = getTeamExtraRuns;