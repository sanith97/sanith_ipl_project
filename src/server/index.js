const matchesPerYear = require('../public/output/matchesPerYear.json');
const matchesWonPerTeam = require('../public/output/matchesWonPerTeam.json');
const extraRunsPerTeam = require('../public/output/extraRuns.json');
const economicalBowlers = require('../public/output/economicalBowlers.json');
const tossAndMatchesWon = require('../public/output/tossAndMatchesWon.json');
const playerOfTheMatch = require('../public/output/playerOfTheMatch.json');
const batsmanStrikeRate = require('../public/output/batsmanStrikeRate.json');
const higestPlayerDismissal = require('../public/output/highestPlayerDismissals.json');
const superOverBestBowler = require('../public/output/superOverBestBowler.json');


//Question 1
document.addEventListener("DOMContentLoaded", function() {

    const myChart = Highcharts.chart('container1', {

        title: {
            text: 'Matches Per Year, 2008-2017'
        },
    
        subtitle: {
            text: 'Data Source - https://www.kaggle.com/manasgarg/ipl'
        },
    
        yAxis: {
            title: {
                text: 'Matches Played'
            }
        },
    
        xAxis: {
            accessibility: {
                rangeDescription: 'Range: 2008 to 2017'
            }
        },
    
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle'
        },
    
        plotOptions: {
            series: {
                label: {
                    connectorAllowed: false
                },
                pointStart: 2008
            }
        },
    
        series: matchesPerYear,
    
        responsive: {
            rules: [{
                condition: {
                    maxWidth: 500
                },
                chartOptions: {
                    legend: {
                        layout: 'horizontal',
                        align: 'center',
                        verticalAlign: 'bottom'
                    }
                }
            }]
        }
    
    });
                      
})


//Question 2
document.addEventListener("DOMContentLoaded", function() {

    const myChart = Highcharts.chart('container2', {
        chart: {
            type: 'column'
        },

        title: {
            text: 'Matches Won by each team per Year, 2008-2017'
        },
    
        subtitle: {
            text: 'Data Source - https://www.kaggle.com/manasgarg/ipl'
        },
    
        yAxis: {
            title: {
                text: 'Number of matches won'
            }
        },
    
        xAxis: {
            accessibility: {
                rangeDescription: 'Range: 2008 to 2017'
            }
        },
    
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle'
        },
    
        plotOptions: {
            series: {
                label: {
                    connectorAllowed: false
                },
                pointStart: 2008
            }
        },
    
        series: matchesWonPerTeam,
    
        responsive: {
            rules: [{
                condition: {
                    maxWidth: 500
                },
                chartOptions: {
                    legend: {
                        layout: 'horizontal',
                        align: 'center',
                        verticalAlign: 'bottom'
                    }
                }
            }]
        }
    
    });
                      
})


//Question 3
document.addEventListener("DOMContentLoaded", function() {

    const myChart = Highcharts.chart('container3', {
        chart: {
            type: 'column'
        },

        title: {
            text: 'Extra Runs Contributed By Each Team in 2016'
        },
    
        subtitle: {
            text: 'Data Source - https://www.kaggle.com/manasgarg/ipl'
        },
    
        yAxis: {
            title: {
                text: 'Extra Runs'
            }
        },
    
        xAxis: {
            title: {
                text: 'Extra Runs in the year 2016'
            }
        },
    
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle'
        },
    
    
        series: extraRunsPerTeam,
    
        responsive: {
            rules: [{
                condition: {
                    maxWidth: 500
                },
                chartOptions: {
                    legend: {
                        layout: 'horizontal',
                        align: 'center',
                        verticalAlign: 'bottom'
                    }
                }
            }]
        }
    
    });
                      
})


//Question 4
document.addEventListener("DOMContentLoaded", function() {

    const myChart = Highcharts.chart('container4', {
        chart: {
            type: 'column'
        },

        title: {
            text: 'Top 10 Economical Bowlers in the year 2015'
        },
    
        subtitle: {
            text: 'Data Source - https://www.kaggle.com/manasgarg/ipl'
        },
    
        yAxis: {
            title: {
                text: 'Economy'
            }
        },
    
        xAxis: {
            title: {
                text: 'Economical Bowlers'
            }
        },
    
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle'
        },
    
    
        series: economicalBowlers,
    
        responsive: {
            rules: [{
                condition: {
                    maxWidth: 500
                },
                chartOptions: {
                    legend: {
                        layout: 'horizontal',
                        align: 'center',
                        verticalAlign: 'bottom'
                    }
                }
            }]
        }
    
    });
                      
})


//Question 5
document.addEventListener("DOMContentLoaded", function() {

    const myChart = Highcharts.chart('container5', {
        chart: {
            type: 'column'
        },

        title: {
            text: 'Toss and Matches won by each team, 2008 to 2017'
        },
    
        subtitle: {
            text: 'Data Source - https://www.kaggle.com/manasgarg/ipl'
        },
    
        yAxis: {
            title: {
                text: 'Toss and Matches Won'
            }
        },
    
        xAxis: {
            title: {
                text: 'Toss and Matches won by Each Team'
            }
        },
    
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle'
        },
    
    
        series: tossAndMatchesWon,
    
        responsive: {
            rules: [{
                condition: {
                    maxWidth: 500
                },
                chartOptions: {
                    legend: {
                        layout: 'horizontal',
                        align: 'center',
                        verticalAlign: 'bottom'
                    }
                }
            }]
        }
    
    });
                      
})


//Question 6
document.addEventListener("DOMContentLoaded", function() {

    const myChart = Highcharts.chart('container6', {
        chart: {
            type: 'column'
        },

        title: {
            text: 'Highest Player Of The Match wons for each Season, 2008 - 2017'
        },
    
        subtitle: {
            text: 'Data Source - https://www.kaggle.com/manasgarg/ipl'
        },
    
        yAxis: {
            title: {
                text: 'Player Of The Match(Number Of Times)'
            }
        },
    
        xAxis: {
            title: {
                text: 'Player of the match for each season'
            }
        },
    
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle'
        },
    
    
        series: playerOfTheMatch,
    
        responsive: {
            rules: [{
                condition: {
                    maxWidth: 500
                },
                chartOptions: {
                    legend: {
                        layout: 'horizontal',
                        align: 'center',
                        verticalAlign: 'bottom'
                    }
                }
            }]
        }
    
    });
                      
})

//Question 7
document.addEventListener("DOMContentLoaded", function() {

    const myChart = Highcharts.chart('container7', {
        chart: {
            type: 'scatter',
            zoomType: 'xy'
        },
        title: {
            text: 'Strike Rates of all batsmans, 2008 - 2017'
        },
        subtitle: {
            text: 'Data Source - https://www.kaggle.com/manasgarg/ipl'
        },
        xAxis: {
            accessibility: {
                rangeDescription: 'Range: 2008 to 2017'
            },
            startOnTick: true,
            endOnTick: true,
            showLastLabel: true
        },
        yAxis: {
            title: {
                text: 'Strike Rate'
            }
        },
        legend: {
            layout: 'vertical',
            align: 'left',
            verticalAlign: 'top',
            x: 100,
            y: 70,
            floating: true,
            backgroundColor: Highcharts.defaultOptions.chart.backgroundColor,
            borderWidth: 1
        },
        plotOptions: {
            scatter: {
                marker: {
                    radius: 5,
                    states: {
                        hover: {
                            enabled: true,
                            lineColor: 'rgb(100,100,100)'
                        }
                    }
                },
                states: {
                    hover: {
                        marker: {
                            enabled: false
                        }
                    }
                },
                tooltip: {
                    headerFormat: '<b>{series.name}</b><br>',
                    pointFormat: '{point.x} %'
                }
            }
        },
        series: batsmanStrikeRate
                      
    })
});


//Question 8
document.addEventListener("DOMContentLoaded", function() {

    const myChart = Highcharts.chart('container8', {
        chart: {
            type: 'column'
        },

        title: {
            text: 'Highest Player dismissals combination from year 2008 to 2017'
        },
    
        subtitle: {
            text: 'Data Source - https://www.kaggle.com/manasgarg/ipl'
        },
    
        yAxis: {
            title: {
                text: 'dismissals'
            }
        },
    
        xAxis: {
            title: {
                text: 'Batsman-Bowler combination dismissals'
            }
        },
    
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle'
        },
    
    
        series: higestPlayerDismissal,
    
        responsive: {
            rules: [{
                condition: {
                    maxWidth: 500
                },
                chartOptions: {
                    legend: {
                        layout: 'horizontal',
                        align: 'center',
                        verticalAlign: 'bottom'
                    }
                }
            }]
        }
    
    });
                      
})


//Question 9 
document.addEventListener("DOMContentLoaded", function() {

    const myChart = Highcharts.chart('container9', {
        chart: {
            type: 'column'
        },

        title: {
            text: 'Best Super Over Bowler, 2008 - 2017'
        },
    
        subtitle: {
            text: 'Data Source - https://www.kaggle.com/manasgarg/ipl'
        },
    
        yAxis: {
            title: {
                text: 'economy'
            }
        },
    
        xAxis: {
            title: {
                text: 'Best Super Over Bowler'
            }
        },
    
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle'
        },
    
    
        series: superOverBestBowler,
    
        responsive: {
            rules: [{
                condition: {
                    maxWidth: 500
                },
                chartOptions: {
                    legend: {
                        layout: 'horizontal',
                        align: 'center',
                        verticalAlign: 'bottom'
                    }
                }
            }]
        }
    
    });
                      
})