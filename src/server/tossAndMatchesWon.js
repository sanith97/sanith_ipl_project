function getTossAndMatchesWon(dataObject) {
    const teams = [
        {
            name : 'Bangalore',
            data : []
        },
        {
            name : 'Kolkata',
            data : []
        },
        {
            name : 'Mumbai',
            data : []
        },
        {
            name : 'Punjab',
            data : []
        },
        {
            name : 'Pune',
            data : []
        },
        {
            name : 'Delhi',
            data : []
        },
        {
            name : 'Gujarat',
            data : []
        },
        {
            name : 'Rajasthan',
            data : []
        },
        {
            name : 'Chennai',
            data : []
        },  
    ];

    //looping over each team to get individual stats
    for(let team = 0; team < teams.length ; team++) {
        let returnedStats = getTeamStats(teams[team]['name']);
        teams[team]['data'].push(returnedStats);
    }

    function getTeamStats(team) {
        let winCount = 0;

        for (let obj of dataObject) {
            if((obj['toss_winner'].includes(team)) && (obj['winner'].includes(team))) {//String.includes()
                winCount += 1;
            }
        }
        return winCount;
    }
    
    //writing this seperately as deccan and hyderabad both are same
    let HyderabadAndDeccan = {
        name : 'Hyderabad',
        data : []
    }
    let winCount = 0

    for(let obj of dataObject) {
        if(((obj['toss_winner'] === 'Deccan Chargers') || obj['toss_winner'] == 'Sunrisers Hyderabad') && ((obj['winner'] === 'Deccan Chargers') || (obj['winner'] === 'Sunrisers Hyderabad'))){
            winCount += 1; 
        }    
    }
    HyderabadAndDeccan['data'].push(winCount);
    
    teams.push(HyderabadAndDeccan);
    
    return teams;
}   

module.exports = getTossAndMatchesWon;