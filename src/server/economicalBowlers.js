function getbolwersList(dataObjectDeliveries, dataObjectMatches, year) {

    if(dataObjectDeliveries === undefined || dataObjectMatches === undefined || year === undefined) {
        return {};
    }

    if(dataObjectDeliveries.length === 0 || dataObjectMatches.length === 0) {
        return {};
    }

    let idObj = dataObjectMatches.filter(obj => obj['season'] == year);
    let idArray = [];

    idObj.forEach(obj => idArray.push(obj['id']));

    let slicedObj = dataObjectDeliveries.filter(obj => (idArray.includes(obj['match_id'])));

    let bowlerArray = [];
    
    //To get bowlers list
    for(let obj of slicedObj) {
        if(bowlerArray.includes(obj['bowler'])) {
            continue;
        } else {
            bowlerArray.push(obj['bowler']);
        }
    }

    let bowlersObj = {};

    // Iterating over each bowler and calling stats function
    for(let bowler of bowlerArray) {
        let returnedData = bowlerStats(bowler);
        bowlersObj[bowler] = returnedData;
    }

    function bowlerStats(bowler) {
        let runs = 0;
        let noOfBalls = 0;

        //if the bowler argument called from above loop is same, then picking up runs and counting bowls
        for(let obj of slicedObj) {
            if(obj['bowler'] == bowler){
                runs += (Number(obj['total_runs']) - Number(obj['legbye_runs']) - Number(obj['bye_runs']) - Number(obj['penalty_runs']));
                if(obj['wide_runs'] == 0 && obj['noball_runs'] == 0) {
                    noOfBalls += 1;
                }
            }
        }

        //converting bowls to overs to get economy

        return Number((runs / (noOfBalls/6)).toFixed(3)); 
        
    }

    //to sort object converting into array
    let sortedBowlersArray = [];

    for(let bowler in bowlersObj) {
        sortedBowlersArray.push({
            name : bowler,
            data : [bowlersObj[bowler]]
        });
    }

    sortedBowlersArray.sort((a,b) => a['data'][0] - b['data'][0]);

    return sortedBowlersArray.slice(0,10);
}


module.exports = getbolwersList;