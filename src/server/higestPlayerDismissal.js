function getHighestPlayerDismissal(dataObject) {

    //getting all dismissal pairs from the data object
    let dismissalArray = [];
    for(let obj of dataObject) {
        if(obj['player_dismissed'].length > 1) {
            dismissalArray.push([obj['batsman'], obj['bowler']]);
        } 
    }

    //mainting array to count the frequency of pairs
    let dismissalCount = [];
    for(let individual of dismissalArray) {
        let batsman = individual[0];
        let bowler = individual[1];
        let count = 0;
        for(let whole of dismissalArray) {
            if(whole[0] === batsman && whole[1] === bowler) {
                count += 1;
            }
        }

        dismissalCount.push(count);
    }
    
    //since we require maximum count 
    let maxDismissals = dismissalCount.reduce((a,b) => Math.max(a,b));

    //i have frequency array but to find index of array where it is matching with max count
    let arrayIndexes = [];
    for(let index = 0; index < dismissalCount.length; index++) {
        if(dismissalCount[index] === maxDismissals) {
            arrayIndexes.push(index)
        }
    }

    //once i have array indexes loopping to get the max count array pairs
    let comboArray = [];
    for(let index of arrayIndexes) {
        comboArray.push(dismissalArray[index]);
    }

    //since combo array contains duplicates used below three steps to remove duplicates
    let stringArray = comboArray.map(JSON.stringify);
    let uniqueStringArray = new Set(stringArray);
    let uniqueArray = Array.from(uniqueStringArray, JSON.parse);


    let requiredObj = {}
    for(let index = 0; index < (comboArray.length / maxDismissals); index++) {
        let key = "pair" + (index + 1);
        let batsman = uniqueArray[index][0];
        let bowler = uniqueArray[index][1];

        requiredObj[key] = {
            'name': `${batsman} - ${bowler}`,
            'data' : [maxDismissals]
        }
        
    }

    let highChartData = [];

    for(let key in requiredObj) {
        highChartData.push(requiredObj[key]);
    }

    return highChartData;
}

module.exports = getHighestPlayerDismissal;

