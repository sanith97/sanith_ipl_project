function getbestBowlerInSuperOver(dataObject) {
    
    let superBowlers = [];

    for(let obj of dataObject) {
        if(obj['is_super_over'] == 1) {
            if(superBowlers.includes(obj['bowler'])) {
                continue;
            } else {
                superBowlers.push(obj['bowler']);
            }
            
        }
    }
    
    let economyArray = [];
    for(let bowler of superBowlers) {
        let runs = 0;
        let balls = 0;
        for(let obj of dataObject) {
            if(obj['bowler'] === bowler && obj['is_super_over'] == 1) {
                runs += (Number(obj['total_runs']) - Number(obj['bye_runs']) - Number(obj['legbye_runs']) - Number(obj['penalty_runs']));
                if(obj['wide_runs'] == 0 && obj['noball_runs'] == 0) {
                    balls += 1;
                }

            }
        }

        let economy = Number((runs / (balls/6)).toFixed(2));
        economyArray.push([bowler, economy]);
    }

    let economicalSuperBowlerArray = economyArray.sort((a,b) => a[1] - b[1])
    let superEconomy = economicalSuperBowlerArray[0][1];

    let superObj = {};

    for(let index of economicalSuperBowlerArray) {
        if(index[1] == superEconomy) {
            superObj[index[0]] = superEconomy;
        }
    }
    return [{
        name : 'JJ Bumrah',
        data : [null,null,null,superObj['JJ Bumrah'],null,null,null]
    }];
}

module.exports = getbestBowlerInSuperOver;