const csvjson = require('csvjson');
const fs = require('fs');
const path = require('path');

const matchesData = fs.readFileSync(path.join(__dirname, '../data/matches.csv'), { encoding : 'utf8'});
const deliveriesData = fs.readFileSync(path.join(__dirname, '../data/deliveries.csv'), { encoding : 'utf8'});

const optionsForMatchesData = {
  delimiter : ',',
  quote     : '"',
};

const optionsForDeliveriesData = {
    delimiter : ',',
    quote     : '"',
  };
 
const matchesObj = csvjson.toObject(matchesData, optionsForMatchesData);
const deliveriesObj = csvjson.toObject(deliveriesData, optionsForDeliveriesData);


const result1 = require('./matchesPerYear.js');
const data1 = JSON.stringify(result1(matchesObj));

let path1 = "/home/sanith/IPLProject/src/public/output/";

fs.writeFile(path1 + "matchesPerYear.json",data1, (e) => {
  if(e) {
    console.log(e);
  }
  else {
    console.log("File written successfully");
  }
})

const result2 = require('./matchesWonPerTeam.js');
const data2 = JSON.stringify(result2(matchesObj));

let path2 = "/home/sanith/IPLProject/src/public/output/";

fs.writeFile(path2 + "matchesWonPerTeam.json",data2, (e) => {
  if(e) {
    console.log(e);
  }
  else {
    console.log("File written successfully");
  }
})


const result3 = require('./extraRuns.js');
const data3 = JSON.stringify(result3(deliveriesObj,matchesObj,2016));

let path3 = "/home/sanith/IPLProject/src/public/output/";

fs.writeFile(path3 + "extraRuns.json",data3, (e) => {
  if(e) {
    console.log(e);
  }
  else {
    console.log("File written successfully");
  }
})

const result4 = require('./economicalBowlers.js');
const data4 = JSON.stringify(result4(deliveriesObj,matchesObj,2015));

let path4 = "/home/sanith/IPLProject/src/public/output/";

fs.writeFile(path4 + "economicalBowlers.json",data4, (e) => {
  if(e) {
    console.log(e);
  }
  else {
    console.log("File written successfully");
  }
})

const result5 = require('./tossAndMatchesWon.js');
const data5 = JSON.stringify(result5(matchesObj));

let path5 = "/home/sanith/IPLProject/src/public/output/";

fs.writeFile(path5 + "tossAndMatchesWon.json",data5, (e) => {
  if(e) {
    console.log(e);
  }
  else {
    console.log("File written successfully");
  }
})

const result6 = require('./playerOfTheMatch.js');
const data6 = JSON.stringify(result6(matchesObj));

let path6 = "/home/sanith/IPLProject/src/public/output/";

fs.writeFile(path6 + "playerOfTheMatch.json",data6, (e) => {
  if(e) {
    console.log(e);
  }
  else {
    console.log("File written successfully");
  }
})


const result7 = require('./batsmanStrikeRate.js');
const data7 = JSON.stringify(result7(deliveriesObj,matchesObj));

let path7 = "/home/sanith/IPLProject/src/public/output/";

fs.writeFile(path7 + "batsmanStrikeRate.json",data7, (e) => {
  if(e) {
    console.log(e);
  }
  else {
    console.log("File written successfully");
  }
})


const result8 = require('./higestPlayerDismissal.js');
const data8 = JSON.stringify(result8(deliveriesObj));

let path8 = "/home/sanith/IPLProject/src/public/output/";

fs.writeFile(path8 + "highestPlayerDismissals.json",data8, (e) => {
  if(e) {
    console.log(e);
  }
  else {
    console.log("File written successfully");
  }
})


const result9 = require('./superOverBestBowler');
const data9 = JSON.stringify(result9(deliveriesObj));

let path9 = "/home/sanith/IPLProject/src/public/output/";

fs.writeFile(path9 + "superOverBestBowler.json",data9, (e) => {
  if(e) {
    console.log(e);
  }
  else {
    console.log("File written successfully");
  }
})
