function getBatsmanStrikeRate(dataObjectDeliveries, dataObjectMatches) {

    if(dataObjectDeliveries === undefined || dataObjectMatches === undefined || dataObjectDeliveries.length === 0 || dataObjectMatches.length === 0) {
        return {};
    }
    
    let seasons = {
        2008: 0,
        2009: 0,
        2010: 0,
        2011: 0,
        2012: 0,
        2013: 0,
        2014: 0,
        2015: 0,
        2016: 0,
        2017: 0,
    }

    let strikeRate = []; // return seasons if old code needed
    let colorCodes = ['rgb(245, 178, 178)', 'rgb(245, 212, 178)', 'rgb(245, 245, 178)', 'rgb(212, 245, 178)', 'rgb(178, 245, 178)', 'rgb(178, 245, 212)', 'rgb(178, 245, 245)', 'rgb(178, 212, 245)', 'rgb(178, 178, 245)', 'rgb(212, 178, 245)'];
    let count = 0;
    //mainting loop to return data year wise
    for(let year of Object.keys(seasons)) {
        let returnedArray = getBatsmanStats(year);
        strikeRate.push({
            name : year,
            color : colorCodes[count],
            data : returnedArray
        })
        count += 1;
    }

    function getBatsmanStats(year) {

        //picking id's from the matches object for particular year
        let idArray = [];
        dataObjectMatches.forEach((obj) => {
            if(obj['season'] == year) {
                idArray.push(obj['id']);
            }
        });
        
        //shortening deliveries object as per the ids captured in the above array
        let yearObj = dataObjectDeliveries.filter((obj) => idArray.includes(obj['match_id']));

        //maintaining array to pick all the batsman's played in particular year
        let batsmanArray = [];
        for(let obj of yearObj) {
            if(batsmanArray.includes(obj['batsman'])) {
                continue;
            } else {
                batsmanArray.push(obj['batsman']);
            }
        }

        
        let batsmanObj = {};
        let strikeRateArray = [];
        //in this loop for each batsman of particular year calculating the runs and balls of the shorted object
        for(batsman of batsmanArray) {
            let runs = 0;
            let balls = 0;
            for(obj of yearObj) {
                if(batsman == obj['batsman']) {
                    runs += Number(obj['batsman_runs']);
                    if(obj['wide_runs'] == 0) {
                        balls += 1;
                    }
                }
            }
            strikeRateArray.push(Number(((runs / balls) * 100).toFixed(2)))
            //added to batsmanobj and fixing the decimal
            batsmanObj[batsman] = Number(((runs / balls) * 100).toFixed(2));
        }

        return strikeRateArray;
    }
    
    return strikeRate;
}

module.exports = getBatsmanStrikeRate;