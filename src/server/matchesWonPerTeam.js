function getMatchesWonPerTeam(dataObject) {
    const teams = [
        {
            name : 'Bangalore',
            data : []
        },
        {
            name : 'Kolkata',
            data : []
        },
        {
            name : 'Mumbai',
            data : []
        },
        {
            name : 'Punjab',
            data : []
        },
        {
            name : 'Pune',
            data : []
        },
        {
            name : 'Delhi',
            data : []
        },
        {
            name : 'Gujarat',
            data : []
        },
        {
            name : 'Rajasthan',
            data : []
        },
        {
            name : 'Chennai',
            data : []
        },  
    ];

    //looping over each team to get individual stats
    for(let team = 0; team < teams.length; team++) {
        let returnedStats = getTeamStats(teams[team]['name']);
        teams[team]['data'] = returnedStats;
    }


    function getTeamStats(team) {
        let startingYear = 2008;
        let emptyArray = [];

        for (let index = 0; index < 10; index++) {
            let winCount = 0;

            for (let obj of dataObject) {
                if((obj['season'] == startingYear) && (obj['winner'].includes(team))) {//String.includes()
                    winCount += 1;
                }
            }
            emptyArray.push(winCount);
            startingYear += 1;
        }
        return emptyArray;
    }

    //writing this seperately as deccan and hyderabad both are same
    let HyderabadAndDeccan = [];
    let yearStart = 2008;

    for(let index = 0; index < 10; index++){
        let winCount = 0
        for(let obj of dataObject) {
            if((obj['season'] == yearStart) && ((obj['winner'] === 'Deccan Chargers') || (obj['winner'] === 'Sunrisers Hyderabad'))){
                winCount += 1; 
            }    
        }
        HyderabadAndDeccan.push(winCount);
        yearStart +=1 ;
    }

    teams.push({
         name : 'Hyderabad',
         data : HyderabadAndDeccan
        });
    
    return teams;

}   

module.exports = getMatchesWonPerTeam;