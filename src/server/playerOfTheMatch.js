function getPlayerOfTheMatch(dataObject) {

    if(dataObject === undefined || dataObject.length === 0) {
      return {};
    }

    let seasons = {
      2008: 0,
      2009: 0,
      2010: 0,
      2011: 0,
      2012: 0,
      2013: 0,
      2014: 0,
      2015: 0,
      2016: 0,
      2017: 0
    };
    
    //looping over each year to get seperate statistics
    for(let year of Object.keys(seasons)) {
      let returnedVal = getPlayerStats(year);
      seasons[year] = returnedVal;
    }

    function getPlayerStats(year) {
      let playersArray = [];
      
      //getting total player names of particular year who has won Player of match atleast once
      for(let obj of dataObject) {
          if(obj['season'] == year) {
            playersArray.push(obj['player_of_match']);
          }
      }
      
      //mainting frequency of each players, basically how many times he won the player of the match
      let countArray = [];
      for(let player of playersArray) {
        let countToBePushed = 0;
        let myPlayer = player;
        for(let samePlayer of playersArray) {
          if(myPlayer === samePlayer) {
            countToBePushed += 1;
          }
        }
        countArray.push(countToBePushed);
      }

      //since i have frequency array, iam picking the max of it
      let maxCount = countArray.reduce((a,b) => Math.max(a,b));

      //mainting separate array, i have max count, picking up indexes of playersArray of max count
      let indexArray = [];
      for(let index = 0; index < countArray.length; index++){
        if(countArray[index] === maxCount) {
          indexArray.push(index);
        }
      }

      //I have player indexes, player may be one or two or more, mainting an array to keep names
      let playerObj = {};
      let requiredPlayers = [];
      
      for(let index of indexArray) {
        if(requiredPlayers.includes(playersArray[index])) {
          continue;
        } else {
          requiredPlayers.push(playersArray[index]);
        }
      }

      //finally i have player names and max count of player of match, pushing them into object
      playerObj['name'] = requiredPlayers.join(",") + ' in ' + year ;
      playerObj['data'] = [maxCount];
      
      return playerObj;
      
    }

    let highChartData = [];
    for(let key in seasons) {
      highChartData.push(seasons[key]);
    }

    return highChartData;
}

module.exports = getPlayerOfTheMatch;




